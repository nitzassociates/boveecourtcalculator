package com.nitzassociates.boveecourtcalculator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class FileListAdapter extends ArrayAdapter<com.nitzassociates.boveecourtcalculator.FileListData> {
	private final Context context;
	private final ArrayList<com.nitzassociates.boveecourtcalculator.FileListData> fileArrayList;

	/*****************************************************************************
	 *	Formatters
	 *****************************************************************************/
	private static SimpleDateFormat fileDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
	private static DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
	private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);

	public FileListAdapter(Context context, ArrayList<com.nitzassociates.boveecourtcalculator.FileListData> fileArrayList) {
		super(context, R.layout.adapter_file_list, fileArrayList);
		 
		this.context = context;
		this.fileArrayList = fileArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = null;
		if(!fileArrayList.get(position).isGroupHeader()) {
			rowView = inflater.inflate(R.layout.file_list_item, parent, false);
			 
			TextView nameView = (TextView) rowView.findViewById(R.id.tv_list_name);
			TextView dateView = (TextView) rowView.findViewById(R.id.tv_list_date);
			TextView amountView = (TextView) rowView.findViewById(R.id.tv_list_amount);

			// Set name
			nameView.setText(fileArrayList.get(position).getName());

			// Set date
			Date dateJudgment = fileDateFormat.parse(fileArrayList.get(position).getDate(), new java.text.ParsePosition(0));
			dateView.setText(dateFormat.format(dateJudgment));

			// Set amount
			double balancePrincipal = Double.parseDouble(fileArrayList.get(position).getAmount());
			amountView.setText(currencyFormat.format(balancePrincipal));

			ImageView imgView = (ImageView) rowView.findViewById(R.id.iv_icon); 
			imgView.setImageResource(fileArrayList.get(position).getIconRes());
		} else {
			rowView = inflater.inflate(R.layout.file_list_header, parent, false);
			TextView titleView = (TextView) rowView.findViewById(R.id.header);
			titleView.setText(fileArrayList.get(position).getName());
			rowView.setClickable(false);
		}
		return rowView;
	}
}
