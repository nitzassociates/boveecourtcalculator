package com.nitzassociates.boveecourtcalculator;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileIo {

	// default filenames
	public static final String JUDGMENT_EXT = ".ini";
	public static final String DEFAULT_FILENAME = "default.ini";
	private static final String INITIAL_FILENAME = "sample.ini";
	private static final String BOVEE_FILENAME = "bovee.ini";

	/************************************************************************
	 *  Create file list array from files with extension: ext				*
	 *  	input 	icon resource ID										*
	 *  			file extension											*
	 *		returns array list												*
	 ************************************************************************/
	public static ArrayList<com.nitzassociates.boveecourtcalculator.FileListData> getJudgmentUserFileInfo(Context ctx, String ext, int iconResId) {
		ArrayList<com.nitzassociates.boveecourtcalculator.FileListData> arrayList = new ArrayList<com.nitzassociates.boveecourtcalculator.FileListData>();
		com.nitzassociates.boveecourtcalculator.FileListFilter fileFilter = new com.nitzassociates.boveecourtcalculator.FileListFilter(null, ext);
		File [] fileList = FileIo.getFileDir(ctx).listFiles(fileFilter);
		for(File filename : fileList) {
			String input = readFile(filename);
			String thisName = com.nitzassociates.boveecourtcalculator.MinIni.getProfileString("Judgment", "name", null, input);
			String thisDate = com.nitzassociates.boveecourtcalculator.MinIni.getProfileString("Judgment", "date", null, input);
			String thisAmount = com.nitzassociates.boveecourtcalculator.MinIni.getProfileString("Judgment", "amount", null, input);
			arrayList.add(new com.nitzassociates.boveecourtcalculator.FileListData(filename.getName(), thisName, thisDate, thisAmount, iconResId));
		}
		return arrayList;
	}

	/************************************************************************
	 *  Create file list array from contents of zip file resource			*
	 *  	input 	icon resource ID										*
	 *  			zip file resource ID									*
	 *		returns array list												*
	 ************************************************************************/
	public static ArrayList<com.nitzassociates.boveecourtcalculator.FileListData> getJudgmentDemoFileInfo(Context ctx, int fileResId, int iconResId) {
		ArrayList<com.nitzassociates.boveecourtcalculator.FileListData> arrayList = new ArrayList<com.nitzassociates.boveecourtcalculator.FileListData>();
		InputStream inStream = ctx.getResources().openRawResource(fileResId);
		ZipInputStream zis = new ZipInputStream (new BufferedInputStream(inStream));
		ZipEntry ze;
		try {
			while ((ze = zis.getNextEntry()) != null) {			// ignore header.ini and default file
				if (!((ze.getName().equalsIgnoreCase("header.txt")) ||
						(ze.getName().equalsIgnoreCase(DEFAULT_FILENAME)))) {
					String input = readZipFile(zis);
					String thisName = com.nitzassociates.boveecourtcalculator.MinIni.getProfileString("Judgment", "name", null, input);
					String thisDate = com.nitzassociates.boveecourtcalculator.MinIni.getProfileString("Judgment", "date", null, input);
					String thisAmount = com.nitzassociates.boveecourtcalculator.MinIni.getProfileString("Judgment", "amount", null, input);
					arrayList.add(new com.nitzassociates.boveecourtcalculator.FileListData(ze.getName(), thisName, thisDate, thisAmount, iconResId));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			zis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return arrayList;
	}

	private static String readZipFile(ZipInputStream zis) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte [] buffer = new byte[1024];
		int count;
		try {
			while ((count = zis.read(buffer)) != -1) {
				baos.write(buffer, 0, count);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toString();
	}

	/************************************************************************
	 *  Read zip file from resource											*
	 *  	input 	zip file resource ID									*
	 *  			zip file name											*
	 *		returns file contents as string									*
	 ************************************************************************/
	public static String readZipFileFromResource(Context ctx, int resId, String fileName) {
		InputStream inStream = ctx.getResources().openRawResource(resId);
		ZipInputStream zis = new ZipInputStream (new BufferedInputStream(inStream));
		ZipEntry ze;
		String outString = null;
		try {
			while ((ze = zis.getNextEntry()) != null) {
				if (ze.getName().equalsIgnoreCase(fileName)) { 
					outString = readZipFile(zis);
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			zis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return outString;
	}

	/************************************************************************
	 *  Read file from default directory									*
	 *  	input 	filename												*
	 *  			extension												*
	 *		returns file contents as a string								*
	 ************************************************************************/
	public static String readFileData(Context ctx, String fileName, String ext) {
		String outString = null;
		com.nitzassociates.boveecourtcalculator.FileListFilter fileFilter = new com.nitzassociates.boveecourtcalculator.FileListFilter(null, ext);
		File [] fileList = getFileDir(ctx).listFiles(fileFilter);
		for(File filename : fileList) {
			if(filename.getName().equalsIgnoreCase(fileName)) {
				outString = readFile(filename);
			}
		}
		return outString;
	}

	public static String readFile(File filename) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			FileInputStream inStream = new FileInputStream(filename); 
			BufferedInputStream bufInStream = new BufferedInputStream(inStream);
	
			byte [] buffer = new byte[1024];
			int count;
			while ((count = bufInStream.read(buffer)) != -1) {
				baos.write(buffer, 0, count);
			}
			bufInStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toString();
	}

	private static SimpleDateFormat filename1Sdf = new SimpleDateFormat("yyMMdd-HHmmss", Locale.US);

	/********************************************************************
	 *	Create filename from date and time								*
	 ********************************************************************/
	public static String makeUniqueFilename1(String ext) {
		String filename = filename1Sdf.format(new Date());
		filename += ext;
		return filename.toLowerCase(Locale.US);
	}

	private static SimpleDateFormat filenameSdf = new SimpleDateFormat("yyMMddHHmmss", Locale.US);

	public static String makeUniqueFilename(String filename, String ext) {
		filename = filename.replaceAll("\\s+", "_");			// replace spaces with underscore
		filename = filename.replaceAll("[^a-zA-Z0-9_]", "");
		filename += filenameSdf.format(new Date());
		filename += ext;
		return filename.toLowerCase(Locale.US);
	}

	public static String getTitle (String inputText) {
		String [] lines = inputText.split("\n");
		String listingTitle = "";
		
		String thisLine;
		String [] splitLine = null;

		for (int jj=0; jj<lines.length; jj++) {
			// get rid of white space
			thisLine = lines[jj];
			thisLine = thisLine.replaceAll("\\s", "\t");
			thisLine = thisLine.replaceAll("\\t+", ";");
			thisLine = thisLine.replaceAll("\r", "");
			// split fields
			splitLine = thisLine.split(";");
			if (splitLine.length >= 2) {		// compound line (format is assumed)
				if(splitLine[0].equalsIgnoreCase("TITLE")) {
					// add the rest of the line
					listingTitle += splitLine[1];
					for (int ii=2; ii<splitLine.length; ii++) {
						listingTitle += " ";
						listingTitle += splitLine[ii];
					}
					break;
				}
			}
		}
		return listingTitle;
	}

	public static File getFileDir(Context context) {
		File file;
		if(getExternalStorageState()) {
			// get location on SD card
			file = context.getExternalFilesDir(null);
		}
		else {
			// get internal storage location
			file = context.getFilesDir();
		}
		return file;
	}

	// check for external storage
	private static boolean getExternalStorageState() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

}
