package com.nitzassociates.boveecourtcalculator;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.util.Calendar;

/**
 * Created by Frederic Nitz on 11/6/2017.
 */

public class DatePickerFragment extends DialogFragment {
    final Calendar thisCalendar = Calendar.getInstance();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        int year = thisCalendar.get(Calendar.YEAR);
        int month = thisCalendar.get(Calendar.MONTH);
        int day = thisCalendar.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog (getActivity(), (DatePickerDialog.OnDateSetListener)getActivity(), year, month, day);
    }

    // Set calendar date
    public void setDate (int year, int month, int day) {
        thisCalendar.set(year, month, day);
    }
}
