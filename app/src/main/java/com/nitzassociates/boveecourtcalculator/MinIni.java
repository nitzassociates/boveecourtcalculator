package com.nitzassociates.boveecourtcalculator;

import java.util.Scanner;

public class MinIni {

	public static String getProfileString(String section, String key, String def, String input) {
		String retStr = new String();
		boolean sectionFound = false;
		boolean keyFound = false;
		String tempSection = "[" + section + "]";
		Scanner s = new Scanner(input);
		String thisLine;
		if(input != null) {
			try {
				while(!(sectionFound & keyFound)) {
					if (s.hasNextLine()) {
						thisLine = s.nextLine();
                        // if thisLine has a continuation character at the end, append the next line(s)
                        while (thisLine != null && thisLine.length() > 0 && thisLine.charAt(thisLine.length() - 1) == '\\') {
                            thisLine = thisLine.substring(0, thisLine.length() - 1);
                            if (s.hasNextLine()) {
                                thisLine += s.nextLine();
                            } else {
                                break;
                            }
                        }

						// delete comments
						int start = thisLine.indexOf('#');
						if(start != -1) {
							thisLine = thisLine.substring(0, start);
						}

						// delete white space
						thisLine = thisLine.replaceFirst("\\s+$", "");

						if(thisLine.contains(tempSection)) {
							sectionFound = true;
							while(!keyFound) {
								if (s.hasNextLine()) {
									thisLine = s.nextLine();
                                    // if thisLine has a continuation character at the end, append the next line(s)
                                    while (thisLine != null && thisLine.length() > 0 && thisLine.charAt(thisLine.length() - 1) == '\\') {
                                        thisLine = thisLine.substring(0, thisLine.length() - 1);
                                        if (s.hasNextLine()) {
                                            thisLine += s.nextLine();
                                        } else {
                                            break;
                                        }
                                    }

                                    // delete comments
									start = thisLine.indexOf('#');
									if(start != -1) {
										thisLine = thisLine.substring(0, start);
									}

									// delete white space
									thisLine = thisLine.replaceFirst("\\s+$", "");

									if(thisLine.contains("[")) break;
									if(thisLine.contains(key + '=')) {
										start = thisLine.indexOf('=')+1;
										retStr = thisLine.substring(start);
										keyFound = true;
									}
								} else {
									keyFound = true;
									retStr = def;
								}
							}
						}
					} else {
						sectionFound = true;
						keyFound = true;
						retStr = def;
					}
				}
			} finally {
				s.close();
			}
		}
		return retStr;
	}

/*
	public static String stripComments (String input) {
		String outBuffer = null;
		Scanner s = new Scanner(input);
		String thisLine;
		boolean test = true;
		while(test) {
			try {
				thisLine = s.nextLine();
				thisLine.trim();
				thisLine.replaceAll("\\s+","");
				thisLine.replaceAll("#+","");
				int start = thisLine.indexOf('#');
				if(start != -1) {
					outBuffer += thisLine.substring(0, start);
				}
			} finally {
				s.close();
				test = false;
			}
		}
		return outBuffer; 
	}
*/
/*
int	WritePrivateProfileString( char *pSection, char *pKey, char *pValue, char *pFileName ){
	FILE		*pSysIniFile;
	FILE		*pTempFile;
	
	char		ReadStr[80];
	char		WriteStr[80];
	char		FileKey[80];
	char		TempKey[80];
	char		TempSection[80];
	int			RetVal;
	char		*pEqualLoc;
	
	int		KeyLen = strlen( pKey );
	int		FileKeyLen;
	int		FirstFlag = TRUE;

	_splitpath( pFileName, TempDrive, TempDir, TempFileName, TempExt );
	_makepath( TempPath, TempDrive, TempDir, TempFileName, "TMP" );

   strcpy( ReadStr, "" );

	strcpy( TempSection, "[" );
	strcat( TempSection, pSection );
	strcat( TempSection, "]" );

	if( pTempFile = fopen( TempPath, "wt" ) ){
		if( pSysIniFile = fopen( pFileName, "rt" ) ){
			while( 1 ){
				RetVal = fscanf( pSysIniFile, "%[^\n]\n", ReadStr );
				if( RetVal != EOF ){
					if( RetVal == 0 ){				// This happens if the first lines are empty
						RetVal = fscanf( pSysIniFile, "\n", ReadStr );
						write_ini( FALSE, "", pTempFile );
					}else{
						if( (ReadStr[0] == '[') && !FirstFlag ) write_ini( TRUE, ReadStr, pTempFile );
						else write_ini( FALSE, ReadStr, pTempFile );
						FirstFlag = FALSE;
						if( strcmp( TempSection, ReadStr ) == 0 ){
							while( 1 ){									// Found the section
								RetVal = fscanf( pSysIniFile,"%[^\n]\n", ReadStr );
								if( (RetVal == EOF) || (ReadStr[0] == '[') ){
                           			// new section or EOF means we didn't find our key
                           			// so we'll add it to the end of this section
									make_key_str( WriteStr, pKey, pValue );
									write_ini( FALSE, WriteStr, pTempFile );

									// if we found a new section
									// write the section label out
									if( RetVal != EOF )
										write_ini( TRUE, ReadStr, pTempFile );
                  					break;
								}else{		// check the key
									strcpy( FileKey, ReadStr );
									if( pEqualLoc = strchr( FileKey, '=' ) ){
										*pEqualLoc = '\0';
										strcpy( TempKey, pKey );
										FileKeyLen = strlen( FileKey );
										if( FileKeyLen > KeyLen ){
											// pad the input key with spaces
											for( int ii = KeyLen; ii < FileKeyLen; ii++ ) TempKey[ii] = ' ';
											TempKey[FileKeyLen] ='\0';
										}
										if( strcmp( FileKey, TempKey ) == 0 ){ // found the key
											strcat( FileKey, "=" );
											strcat( FileKey, pValue );	// add in the replacement value
											write_ini( FALSE, FileKey, pTempFile );
											break;
										}else write_ini( FALSE, ReadStr, pTempFile );
									}else write_ini( FALSE, ReadStr, pTempFile );
								}
							}
							break;
						}
					}
				}else{		// didn't find the section so we'll create it at the end of the file
					write_ini( TRUE, TempSection, pTempFile );
					make_key_str( WriteStr, pKey, pValue );	// now write the key
					write_ini( FALSE, WriteStr, pTempFile );
					break;
				}
			}
			if( RetVal != EOF ){	// Copy rest of file
				while( 1 ){
					RetVal = fscanf( pSysIniFile, "%[^\n]\n", ReadStr );
					if( RetVal == EOF ) break;
					if( (ReadStr[0] == '[') && !FirstFlag ) write_ini( TRUE, ReadStr, pTempFile );
					else write_ini( FALSE, ReadStr, pTempFile );
					FirstFlag = FALSE;
				}
			}
			fclose( pSysIniFile );
		}else{		// ini file doesn't exist
			write_ini( FALSE, TempSection, pTempFile );
			make_key_str( WriteStr, pKey, pValue );
			write_ini( FALSE, WriteStr, pTempFile );
		}
		fclose( pTempFile );

		remove( pFileName );
		rename( TempPath, pFileName );
		return 0;
	}
	return 1;
}

	char	*make_key_str( char *pOut, char *pKey, char *pValue )
	{
		strcpy( pOut, pKey );
		strcat( pOut, "=" );
		strcat( pOut, pValue );
		return pOut;
	}
	
	int write_ini( int Flag, char *pOut, FILE *pFile )
	{
	char	Work[80];
	int	RetVal;
	
		if( Flag ) strcpy( Work, "\n" );
		else strcpy( Work, "" );
		strcat( Work, pOut );
		strcat( Work, "\n" );
		RetVal = fwrite( Work, sizeof( char ), strlen(Work), pFile );
		return RetVal;
	}
*/

}
