package com.nitzassociates.boveecourtcalculator;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class PaymentData implements Parcelable {
    private long date;
//    private Calendar date;
    private double payment;
    private double cost;

    public PaymentData (long date, double payment, double cost) {
        this.date = date;
        this.payment = payment;
        this.cost = cost;
    }

    public PaymentData (Calendar date, double payment, double cost) {
        this.date = date.getTimeInMillis();
        this.payment = payment;
        this.cost = cost;
    }

/*    public Calendar getDate() {
        return date;
    }
*/
    public long getDate () { return date; }
    public double getPayment() {
        return payment;
    }
    public double getCost() {
        return cost;
    }

/*
    public void setDate (Calendar date) {
        this.date = date;
    }
*/
    public void setDate (long date) {
        this.date = date;
    }

    public void setPayment (double payment) {
        this.payment = payment;
    }

    public void setCost (double cost) {
        this.cost = cost;
    }

    // The following methods that are required for using Parcelable
    private PaymentData(Parcel in) {
        // This order must match the order in writeToParcel()
        date = in.readLong();
        payment = in.readDouble();
        cost = in.readDouble();
    }

    public void writeToParcel(Parcel out, int flags) {
        // This order must match the PaymentData(Parcel) constructor
        out.writeLong(date);
        out.writeDouble(payment);
        out.writeDouble(cost);
    }

    // Just cut and paste this for now
    @Override
    public int describeContents() {
        return 0;
    }

    // Just cut and paste this for now
    public static final Parcelable.Creator<PaymentData> CREATOR = new Parcelable.Creator<PaymentData>() {
        @Override
        public PaymentData createFromParcel(Parcel in) {
            return new PaymentData(in);
        }

        @Override
        public PaymentData[] newArray(int size) {
            return new PaymentData[size];
        }
    };
}
