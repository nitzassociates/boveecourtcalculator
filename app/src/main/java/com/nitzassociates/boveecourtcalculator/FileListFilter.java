package com.nitzassociates.boveecourtcalculator;

import java.io.File;
import java.io.FilenameFilter;

public class FileListFilter implements FilenameFilter {
	private String prefix;
	private String extension;

	public FileListFilter(String prefix, String extension) {
		this.prefix = prefix;
		this.extension = extension;
	}

	public boolean accept(File directory, String filename) {
		boolean fileOK = true;

		if (prefix != null) {
			fileOK &= filename.startsWith(prefix);
		}

		if (extension != null) {
			fileOK &= filename.endsWith(extension);
		}

		return fileOK;
	}
}
