package com.nitzassociates.boveecourtcalculator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Frederic Nitz on 12/6/2017.
 */

public class JudgmentFragment extends Fragment {
    private static final String TAG = JudgmentFragment.class.getSimpleName();
    private static final boolean DEBUG = BuildConfig.DEBUG;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (DEBUG) Log.d(TAG, "onCreate (Bundle)");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (DEBUG) Log.d(TAG, "onCreateView(LayoutInflater, ViewGroup, Bundle)");

//        return inflater.inflate(R.layout.fragment_judgment, container, false);
        return inflater.inflate(R.layout.fragment_judgment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (DEBUG) Log.d(TAG, "onActivityCreated (Bundle)");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        if (DEBUG) Log.d(TAG, "onResume()");
        super.onResume();

    }

    @Override
    public void onPause() {
        if (DEBUG) Log.d(TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (DEBUG) Log.d(TAG, "onSaveInstanceState(Bundle)");
        super.onSaveInstanceState(savedInstanceState);

//        savedInstanceState.putInt(STATE_ORIENTATION, mOrientation);
    }

    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {
        if (DEBUG) Log.d(TAG, "onCreateOptionsMenu(Menu, MenuInflater)");
//        inflater.inflate(R.menu.menu_lighting, menu);
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu) {
        if (DEBUG) Log.d(TAG, "onPrepareOptionsMenu(Menu)");
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        if (DEBUG) Log.d(TAG, "onOptionsItemSelected (MenuItem)");

        return super.onOptionsItemSelected(item);
    }

}
