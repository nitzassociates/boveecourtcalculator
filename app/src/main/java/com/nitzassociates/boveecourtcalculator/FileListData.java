package com.nitzassociates.boveecourtcalculator;

public class FileListData {
	private String name;
	private String date;
	private String amount;
	private int icRes;
	private String fileName;
	private boolean isGroupHeader = false;
	
	public FileListData(String title) {
		this(null, title, null, null, -1);
		isGroupHeader = true;
	}

	public FileListData(String fileName, String name, String date, String amount, int icRes) {
		super();
		this.name = name;
		this.date = date;
		this.amount = amount;
		this.icRes = icRes;
		this.fileName = fileName;
	}
	 
	//getters & setters...
	public String getFileName () {
		return fileName;
	}
	    
	public String getName () {
		return name;
	}
	    
	public String getDate () {
		return date;
	}

	public String getAmount () {
		return amount;
	}

	public int getIconRes() {
		return icRes;
	}

	public boolean isGroupHeader () {
		return isGroupHeader;
	}
}
